### **Hi there ! I'm Md. Hashibur Rahman Kwoshik 👋**
I’m currently an undergraduate student of CSE, RUET (Rajshahi University of Engineering and Technology), Bangladesh 
## Languages I work with
<img src="/svgs/lang/C.svg" alt="C" height="30px"/>
<img src="/svgs/lang/c++.svg" alt="C++" height="30px"/>
<img src="/svgs/lang/python.svg" alt="python" height="30px"/>
<img src="/svgs/lang/java.svg" alt="java" height="30px"/>
<img src="/svgs/lang/ruby.svg" alt="ruby" height="30px"/>
<img src="/svgs/lang/sql_1.svg" alt="sql" height="30px"/>

## Text editors and IDEs I use
<img src="/svgs/ide/geany.svg" alt="geany" height="30px"/>
<img src="/svgs/ide/vscode.svg" alt="vscode" height="30px"/>
<img src="/svgs/ide/vim.svg" alt="vim" height="30px"/>
<img src="/svgs/ide/arduino.svg" alt="arduino" height="30px"/>
<br>
<img src="/svgs/ide/code_blocks.svg" alt="codeblocks" height="30px"/>
<img src="/svgs/ide/android_studio.svg" alt="android_studio" height="30px"/>
<img src="/svgs/ide/pycharm.svg" alt="pycharm" height="30px"/>
<img src="/svgs/ide/intellij.svg" alt="intellij" height="30px"/>

## Platforms and Tools I use
<img src="/svgs/git/git.svg" alt="git" height="30px"/>
<img src="/svgs/git/gitlab.svg" alt="gitlab" height="30px"/>
<img src="/svgs/git/codeberg.svg" alt="codeberg" height="30px"/>
<br>
<img src="/svgs/framework/android.svg" alt="android" height="30px"/>

## Connect with me
[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/facebook.svg' alt='facebook' height='40'>](https://www.facebook.com/rootminusone.rootminusone.3)
[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/protonmail.svg' alt='protonmail' height='40'>](rootminusone8004@protonmail.com)
[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg' alt='gitlab' height='40'>](https://gitlab.com/rootminusone8004)
[<img src='/svgs/codeberg_logo.svg' alt='codeberg' height='40'>](https://codeberg.com/rootminusone8004)
